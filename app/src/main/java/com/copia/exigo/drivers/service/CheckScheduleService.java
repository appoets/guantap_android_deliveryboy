package com.copia.exigo.drivers.service;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import android.os.Build;


@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class CheckScheduleService extends JobService {
    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        if (!isServiceRunning()) {
            startService(new Intent(getApplicationContext(), LocationShareService.class));
            /*if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                startService(new Intent(getApplicationContext(), LocationShareService.class));
            } else {
                Intent serviceIntent = new Intent(getApplicationContext(), LocationShareService.class);
                ContextCompat.startForegroundService(getApplicationContext(), serviceIntent);
            }*/
        } else {
            stopLocationService();
        }
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return true;
    }

    private boolean isServiceRunning() {
        ActivityManager manager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service :
                manager.getRunningServices(Integer.MAX_VALUE)) {
            if (LocationShareService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void stopLocationService() {
        if (isServiceRunning())
            stopService(new Intent(getApplicationContext(), LocationShareService.class));
    }
}
