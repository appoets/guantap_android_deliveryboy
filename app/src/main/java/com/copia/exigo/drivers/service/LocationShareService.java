package com.copia.exigo.drivers.service;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.copia.exigo.drivers.helper.GlobalData;
import com.copia.exigo.drivers.helper.SharedHelper;
import com.copia.exigo.drivers.model.Profile;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.copia.exigo.drivers.helper.GlobalData.CURRENT_LOCATION;


public class LocationShareService extends Service {

    private static final String TAG = "LocationShareService";

    private int UPDATE_INTERVAL_IN_MILLISECONDS = 5000;

    private int FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    private LocationRequest locationRequest = null;
    private LocationSettingsRequest mLocationSettingsRequest = null;
    private LocationCallback mLocationCallback = null;
    private FusedLocationProviderClient mFusedLocationClient = null;
    private SettingsClient mSettingsClient = null;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        buildSettingRequest();
        createLocationCallback();
        return START_STICKY;
    }

    private void buildSettingRequest() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "location provider requires ACCESS_FINE_LOCATION | ACCESS_COARSE_LOCATION");
            return;
        }
        try {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mSettingsClient = LocationServices.getSettingsClient(this);
            locationRequest = new LocationRequest();
            locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
            locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
            builder.addLocationRequest(locationRequest);
            mLocationSettingsRequest = builder.build();
            try {

                mSettingsClient.checkLocationSettings(mLocationSettingsRequest).addOnSuccessListener(settingsResponse -> {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        stopSelf();
                        return;
                    }
                    mFusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper());
                }).addOnFailureListener(e -> {
                    Log.e(TAG, e.toString());
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createLocationCallback() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "location provider requires ACCESS_FINE_LOCATION | ACCESS_COARSE_LOCATION");
            return;
        }
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Log.d(TAG, "locationResult" + locationResult);
                if (locationResult.getLastLocation() != null)
                    updateLocation(locationResult.getLastLocation());
            }
        };
    }



    private void updateLocation(Location mLastLocation) {
        CURRENT_LOCATION=mLastLocation;
        HashMap<String, Double> map = new HashMap<>();
        map.put("latitude", mLastLocation.getLatitude());
        map.put("longitude", mLastLocation.getLongitude());
        Log.d(TAG, "updateLocationToServer "+map.toString());
        String header = SharedHelper.getKey(this, "token_type") + " " + SharedHelper.getKey(this, "access_token");
        Call<Profile> call = GlobalData.api.updateLocation(header, map);
        call.enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(@NonNull Call<Profile> call, @NonNull Response<Profile> response) {
                if (response.errorBody() != null) {

                } else if (response.isSuccessful()) {
                    GlobalData.profile = response.body();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Profile> call, @NonNull Throwable t) {
                Log.d("GPSTracker", "Something wrong - updateLocationToServer");
            }
        });
    }


    private void onStopUpdate() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

   /* @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
       // stopForeground(true);
        onStopUpdate();
        super.onDestroy();
    }*/
}
